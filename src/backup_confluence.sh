#!/bin/bash
# Backup Confluence to Yandex Disk
# To decrypt: gpg -o backup-...zip --batch --yes --passphrase=$PASSPHRASE -d backup-...zip.gpg

CONFLUENCE_BACKUPS=/u01/confluence/backups
CONFLUENCE_YANDEX=/u00/yandex/backups/confluence
PASSPHRASE=...

PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin"
export PATH

# Copy latest backups to Yandex Disk
find $CONFLUENCE_BACKUPS -type f -mmin +60 -print0 | 
    while IFS= read -r -d $'\0' file; do 
        gpg --yes --batch --passphrase=$PASSPHRASE -o $CONFLUENCE_YANDEX/$(basename $file).gpg -c $file
        rm -f $file
    done

# Remove obsolete backups from Yandex Disk (after 10 days)
find $CONFLUENCE_YANDEX -type f -mtime +10 -exec rm -f {} \;

# Done
exit 0

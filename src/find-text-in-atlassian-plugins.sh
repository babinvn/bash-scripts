#!/bin/bash
# Find text string in Atlassian plugins (atlassian-plugins.xml)
# Usage: ./find-text-in-atlassian-plugins.sh <dir> <text>

DIR=${1}
TEXT=${2}

find ${DIR} -type f -iname "*.jar" -print0 | 
    while IFS= read -r -d $'\0' file; do
        unzip -c ${file} "atlassian-plugin.xml" 2>/dev/null | grep -i ${TEXT} >/dev/null
        if [[ $? == 0 ]]; then
            echo ${file}
        fi
    done

# Done
exit 0

#!/bin/bash
# Backup MySQL DB to Yandex Disk
# To decrypt: gpg -o backup-...sql --batch --yes --passphrase=$PASSPHRASE -d backup-...sql.gpg

MYSQL_BACKUPS=/u01/mariadb/sqlbackup
MYSQL_YANDEX=/u00/yandex/backups/mysql

BACKUP_FILE=backup-$(date +%Y%m%d).sql
ROOT_PASSWORD=...
PASSPHRASE=...

PATH="/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin"
export PATH

docker exec mysql sh -c "mysqldump -A -u root -p$ROOT_PASSWORD > /var/lib/mysql/sqlbackup/$BACKUP_FILE" ; RET=$?

if [[ $RET != 0 ]]; then
  echo "MySQL dump failed, exiting..."
  exit $RET
fi

# Copy DB backup to Yandex Disk
gpg --yes --batch --passphrase=$PASSPHRASE -o $MYSQL_YANDEX/$BACKUP_FILE.gpg -c $MYSQL_BACKUPS/$BACKUP_FILE
rm -f $MYSQL_BACKUPS/$BACKUP_FILE

# Remove obsolete backups from Yandex Disk (after 10 days)
find $MYSQL_YANDEX -type f -mtime +10 -exec rm -f {} \;

# Done
exit 0
